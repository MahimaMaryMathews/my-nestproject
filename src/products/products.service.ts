import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Products } from './interface/products.interface';
import { ProductsDTO } from './dto/products.dto';

@Injectable()
export class ProductsService {

    constructor(@InjectModel('Products') private readonly productModel: Model<Products>) { }

    async getAllProducts(): Promise<Products[]> {
        const products = await this.productModel.find().exec();
        return products;
    }

    async getProduct(productID): Promise<Products> {
        const product = await this.productModel.findById(productID).exec();
        return product;
    }

    async addProduct(productsDTO: ProductsDTO): Promise<Products> {
        console.log("Added : " + productsDTO.name);
        const newproduct = await this.productModel(productsDTO);
        return newproduct.save();
    }

    async updateProduct(productID, productsDTO: ProductsDTO): Promise<Products> {
        const updatedProduct = await this.productModel
            .findByIdAndUpdate(productID, productsDTO, { new: true });
        return updatedProduct.save();
    }

    async deleteProduct(productID): Promise<any> {
        const deletedProduct = await this.productModel.findByIdAndRemove(productID);
        return deletedProduct;
    }

}