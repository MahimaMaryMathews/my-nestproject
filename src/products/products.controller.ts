import { Controller, Get, Res, HttpStatus, Post, Body, Put, Query, NotFoundException, Delete, Param, Patch } from '@nestjs/common';
import { ProductsService } from './products.service';
import { ProductsDTO } from './dto/products.dto';

@Controller('products')
export class ProductsController {
    constructor(private productService: ProductsService) { }

    @Post()
    async addProduct(@Res() res, @Body() productsDTO: ProductsDTO) {
        const products = await this.productService.addProduct(productsDTO);
        return res.status(HttpStatus.OK).json({
            message: "Product has been created successfully",
            products
        })
    }

    @Get()
    async getAllProducts(@Res() res) {
        const products = await this.productService.getAllProducts();
        return res.status(HttpStatus.OK).json(products);
    }

    @Get(':productID')
    async getProduct(@Res() res, @Param('productID') productID) {
        const product = await this.productService.getProduct(productID);
        if (!product) throw new NotFoundException('Product does not exist!');
        return res.status(HttpStatus.OK).json(product);
    }

    @Put(':productID')
    async updateProduct(@Res() res, @Param('productID') productID, @Body() productsDTO: ProductsDTO) {
        const product = await this.productService.updateProduct(productID, productsDTO);
        if (!product) throw new NotFoundException('Product does not exist!');
        return res.status(HttpStatus.OK).json({
            message: 'Product has been successfully updated',
            product
        });
    }

    @Delete(':productID')
    async deleteProduct(@Res() res, @Param('productID') productID) {
        const product = await this.productService.deleteProduct(productID);
        if (!product) throw new NotFoundException('Product does not exist');
        return res.status(HttpStatus.OK).json({
            message: 'Product has been deleted',
            product
        })
    }
}