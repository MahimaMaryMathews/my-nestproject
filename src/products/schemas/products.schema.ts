import * as mongoose from 'mongoose';

export const ProductsSchema = new mongoose.Schema({
    name: { type: String, required: true },
    organisation: { type: String, required: true },
    islive: { type: Boolean, required: true },
    description: {
        type: { type: String },
        livefrom: { type: Date },
        liveto: { type: Date },
        days: { type: [] }
    },
    address: {
        street: { type: String },
        city: { type: String },
        pin: { type: String }
    },
    price: {
        amount: { type: Number },
        currency: { type: String },
        discount: { type: Number }
    }
});

/*export const ProductsDocSchema = new mongoose.Schema({
    name: { type: String, required: true },
    products: [ProductsSchema]
});*/