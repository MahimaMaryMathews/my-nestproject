import * as mongoose from 'mongoose';

export class ProductsDTO extends mongoose.Document {
    id: string;
    name: string;
    organisation: string;
    islive: boolean;
    description: {
        type: string,
        livefrom: Date,
        liveto: Date,
        days: []
    };
    address: {
        street: { type: string },
        city: { type: string },
        pin: { type: string }
    };
    price: {
        amount: number,
        currency: string,
        discount: number
    };
}